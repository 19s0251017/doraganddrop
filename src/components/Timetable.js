export default class Timetable {
  constructor() {
    this.daysOfWeek = [
      '月',
      '火',
      '水',
      '木',
      '金',
    ];
    this.komads = [
      {
        time: 1,
        komaArray: [
          { subject: 'abcd', teacher: 'hira' },
          { subject: '' },
          { subject: '' },
          { subject: '' },
          { subject: '' },
        ],
      },
      {
        time: 2,
        komaArray: [
          { subject: '' },
          { subject: '' },
          { subject: '' },
          { subject: '' },
          { subject: '' },
        ],
      },
      {
        time: 3,
        komaArray: [
          { subject: '' },
          { subject: '' },
          { subject: '' },
          { subject: '' },
          { subject: '' },
        ],
      },
      {
        time: 4,
        komaArray: [
          { subject: '' },
          { subject: '' },
          { subject: '' },
          { subject: '' },
          { subject: '' },
        ],
      },
      {
        time: 5,
        komaArray: [
          { subject: '' },
          { subject: '' },
          { subject: '' },
          { subject: '' },
          { subject: '' },
        ],
      },
      {
        time: 6,
        komaArray: [
          { subject: '' },
          { subject: '' },
          { subject: '' },
          { subject: '' },
          { subject: '' },
        ],
      },
    ];
  }

  getKoma(dayOfWeek, time) {
    return this.komads[time - 1].komaArray[dayOfWeek - 1];
  }

  setKoma(dayOfWeek, time, koma) {
    this.komads[time - 1].komaArray.splice(dayOfWeek - 1, 1, koma);
  }

  copyKoma(from, to) {
    const kari = this.getKoma(from.dayOfWeek, from.time);
    this.setKoma(to.dayOfWeek, to.time, kari);
  }
}
